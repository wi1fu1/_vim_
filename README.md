# Конфигурация vim для администратора с поддержкой синтаксиса нескольких языков

## Установка

    cd
    mkdir -p ~/Git ~/.config/nvim/ ~/.local/share/nvim/site/autoload/
    cd Git
    git clone git@bitbucket.org:wi1fu1/_vim_.git
    ln -s Git/_vim_/.vimrc ~/.vimrc
    ln -s Git/_vim_/.vimrc ~/.config/nvim/init.vim
    cd _vim_
    :PlugInstall
