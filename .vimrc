set cpoptions+=$    
set hidden
set endofline
set cindent
set smartindent
set autoindent
set expandtab
set tabstop=4
set shiftwidth=4
set smarttab
set et
set wrap
set linebreak
set iminsert=0
set imsearch=0
set showcmd
set showmatch
set incsearch
set nohlsearch
set termencoding=utf-8
set fdm=syntax
set foldlevel=10
set statusline=%<%f%h%m%r\ %b\ %{&encoding}\ 0x\ \ %l,%c%V\ %P
set laststatus=2
set noswapfile
set complete=""
set complete+=. " Из текущего буфера
set complete+=k " Из словаря
set complete+=b " Из других открытых буферов
set complete+=t  " из тегов
set ic
set backspace=indent,eol,start whichwrap+=<,>,[,]
set whichwrap=b,s,<,>,[,],l,h
set nocompatible
set wildmenu
set wcm=<Tab>

if has('nvim')
    " Neovim specific commands
else
    " Standard vim specific commands
endif

let &errorformat="%f:%l:%c: %t%*[^:]:%m,%f:%l: %t%*[^:]:%m," . &errorformat
let hostname = substitute(system('hostname'), '\n', '', '')

let filename=@%
if has("comment")
    let git_message="VimCommit, changed file: " . filename . "Comment: " . comment
else
    let git_message="VimCommit, changed file: " . filename
endif

if empty(glob("~/.vim/autoload/plug.vim"))
    silent execute '!curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
    Plug 'tpope/vim-fugitive'
    Plug 'saltstack/salt-vim'
    Plug 'msanders/snipmate.vim'
    Plug 'scrooloose/nerdtree'
    Plug 'chr4/nginx.vim'
    Plug 'xolox/vim-misc'
    Plug 'bronson/vim-trailing-whitespace'
    Plug 'ctrlpvim/ctrlp.vim'
    Plug 'mattn/webapi-vim'
    Plug 'plasticboy/vim-markdown'
    Plug 'ErichDonGubler/vim-sublime-monokai'
    Plug 'blueshirts/darcula'
    Plug 'vim-airline/vim-airline'
    Plug 'rodjek/vim-puppet'
    Plug 'meries/puppet-vim'
    Plug 'dense-analysis/ale'
    Plug 'farmergreg/vim-lastplace'
    Plug 'jvirtanen/vim-hcl'
    Plug 'hashivim/vim-terraform'
call plug#end()

" Write this in your vimrc file
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_insert_leave = 0
" You can disable this option too
" if you don't want linters to run on opening a file
let g:ale_lint_on_enter = 0

if hostname == "wilful.h.srv-nix.com"
    colorscheme darcula
    let g:airline_powerline_fonts = 1
    let g:airline#extensions#keymap#enabled = 0
    let g:airline_section_z = "\ue0a1:%l/%L Col:%c"
    let g:Powerline_symbols='unicode'
    let g:airline#extensions#xkblayout#enabled = 0
elseif hostname == "pooh"
endif

set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"

if version >= 700
    set history=64
    set undolevels=128
    set undodir=~/.vim/undodir/
    set undofile
    set undolevels=1000
    set undoreload=10000
    set spelllang=ru,en
    map <F3> :set spell!<CR><bar>:echo "Spell check: " . strpart("OFFON", 3 * &spell, 3)<CR>
endif

let g:ctrlp_extensions = ['tag','buffertag','quickfix','dir','rtscript','undo','line','changes','mixed']
let g:xptemplate_brace_complete = ''

augroup FiletypeDetect
    au! BufRead,BufNewFile /etc/nginx/*,/usr/local/nginx/*,*/nginx/sites-available/*,*/nginx/sites-sites-enabled/*,nginx.conf setfiletype nginx
    au! BufRead,BufNewFile *.markdown,*.md set filetype=liquid
    au! BufRead,BufNewFile *.pp set filetype=puppet
    au! BufRead,BufNewFile *.lua set filetype=lua
augroup END

let g:Lua_OutputMethod = 'vim-qf'
augroup luafiletypes
    autocmd! FileType lua set ai sw=4 sts=4 et
    autocmd FileType lua let g:lua_complete_omni = 1
    autocmd FileType lua let g:lua_inspect_events = ''
augroup END

filetype plugin on

"if has("autocmd")
"    set viewoptions=cursor,folds
"    au BufWinLeave ?* mkview
"    au BufWinEnter ?* silent loadview
"endif

autocmd FileType c,cpp,java,scala let b:comment_leader = '// '
autocmd FileType sh,ruby,python   let b:comment_leader = '# '
autocmd FileType conf,fstab       let b:comment_leader = '# '
autocmd FileType puppet           let b:comment_leader = '# '
autocmd FileType tex              let b:comment_leader = '% '
autocmd FileType mail             let b:comment_leader = '> '
autocmd FileType vim              let b:comment_leader = '" '
autocmd FileType lua              let b:comment_leader = '-- '

noremap <silent> ,cc :<C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
noremap <silent> ,cu :<C-B>silent <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:nohlsearch<CR>
noremap <silent> ,sy :!bash -c "~/bin/sp &>/dev/null "&<CR>

nmap <F2> :w<CR>
vmap <F2> <esc>:w<CR>i
imap <F2> <esc>:w<CR>i

nmap <F4> :Git status<CR>
vmap <F4> <esc>:Git status<CR>
imap <F4> <esc>:Git status<CR>

execute "nmap <F5> :Git commit -a -m'[" . git_message . "]' <CR>"
execute "vmap <F5> :Git commit -a -m'[" . git_message . "]' <CR>"
execute "imap <F5> :Git commit -a -m'[" . git_message . "]' <CR>"

" nmap <F6> :Git push<CR>
" vmap <F6> <esc>:Git push<CR>i
" imap <F6> <esc>:Git push<CR>i

imap <C-t>t <Esc>:tabnew<CR>
nmap <C-t>t :tabnew<CR>

imap <C-t>n <Esc>:tabNext<CR>
nmap <C-t>n :tabNext<CR>

imap <C-t>p <Esc>:tabprevious<CR>
nmap <C-t>p :tabprevious<CR>

menu Encoding.Read.utf-8<Tab><F7> :e ++enc=utf8 <CR>
menu Encoding.Read.windows-1251<Tab><F7> :e ++enc=cp1251<CR>
menu Encoding.Read.koi8-r<Tab><F7> :e ++enc=koi8-r<CR>
menu Encoding.Read.cp866<Tab><F7> :e ++enc=cp866<CR>
map <F7> :emenu Encoding.Read.<TAB>

menu Encoding.Write.utf-8<Tab><S-F7> :set fenc=utf8 <CR>
menu Encoding.Write.windows-1251<Tab><S-F7> :set fenc=cp1251<CR>
menu Encoding.Write.koi8-r<Tab><S-F7> :set fenc=koi8-r<CR>
menu Encoding.Write.cp866<Tab><S-F7> :set fenc=cp866<CR>
map <F8> :emenu Encoding.Write.<TAB>

set wildmenu
set wcm=<Tab>
set cpo-=<
source $VIMRUNTIME/menu.vim
map <F9> :emenu <tab>

map <F10> <Esc>:q<CR>
imap <F10> <Esc>:q<CR>
cmap <F10> <Esc><Esc>:q<CR>

map <S-Insert> <MiddleMouse>

vmap < <gv
vmap > >gv

imap <Ins> <Esc>i

nmap <PageUp> <C-U><C-U>
imap <PageUp> <C-O><C-U><C-O><C-U>
nmap <PageDown> <C-D><C-D>
imap <PageDown> <C-O><C-D><C-O><C-D>

"set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz













